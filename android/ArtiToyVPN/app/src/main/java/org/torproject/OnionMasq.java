package org.torproject;

import android.os.Build;

import androidx.annotation.WorkerThread;

import org.torproject.artitoyvpn.utils.OnionmasqHelper;

import java.io.Closeable;

public class OnionMasq {

    /**
     * Java Holder Class for a rust futures::channel::oneshot::Sender
     * It's responsible for sending the exit signal to an running onionmasq proxy.
     */
    public static class ExitSender implements Closeable {
        // pointer to the rust sender object
        public long exitPointer;

        public ExitSender(long reference) {
            this.exitPointer = reference;
        }

        /**
         * Stops the proxy
         */
        @Override
        public void close() {
            closeProxy(this);
            this.exitPointer = 0;
        }
    }

    /**
     * Starts onionmasq to route all packets over the tun interface into the tor network.
     * This method is blocking and should be called from a worker thread.
     * Always close a running proxy through the ExitSender object.
     * @param filedescriptor the tun interface's filedescriptor
     * @param interfaceName the tun interface name, e.g. tun0
     * @param cacheDir temporary data dir
     * @param dataDir persistent data dir
     * @param exitSender holds a reference to the rust object used to close the proxy. Do not clone the java object otherwise you will leak memory.
     * */
    @WorkerThread
    public static native void runProxy(int filedescriptor, String interfaceName, String cacheDir, String dataDir, ExitSender exitSender);

    /**
     * Stops the proxy.
     * This method is called via ExitSender.close(), don't call it manually.
     * @param exitSender Java holder containing a reference to the rust object used to close the proxy
     */
    public static native void closeProxy(ExitSender exitSender);

    /**
     * Initializes the logcat logging and the Java environment in Rust.
     * Call this method from the main thread and only once during the app's lifecycle.
     */
    public static native void init();

    /**
     * Returns an apps UID from a packets IP source and destination address.
     * This method doesn't block, i.e. no reverse name service lookup is performed.
     * IPv4 address byte array must be 4 bytes long and IPv6 byte array must be 16 bytes long
     * This method can be called from Rust after the Java environment has been initialized ({@link #init()}).
     * @param rawSourceAddress: the raw IP source address in network byte order (the highest order byte of the address is in rawSourceAddress[0]).
     * @param sourcePort: the source IP's port
     * @param rawDestinationAddress: the raw IP destination address in network byte order
     * @param destinationPort: the destination IP's port
     * @return an app's UID or -1 if no UID could be found
     */
    @SuppressWarnings("unused")
    public static int getConnectionOwnerUid(int protocol, byte[] rawSourceAddress, int sourcePort, byte[] rawDestinationAddress, int destinationPort) {
        return OnionmasqHelper.getConnectionOwnerUid(protocol, rawSourceAddress, sourcePort, rawDestinationAddress, destinationPort);
    }


    @SuppressWarnings("unused")
    public static boolean protect(int socket) {
        return OnionmasqHelper.protect(socket);
    }

    /**
     * Returns the Android SDK Version number.
     * This method can be called from Rust after the Java environment has been initialized ({@link #init()}).
     * @return Android SDK Version number
     */
    @SuppressWarnings("unused")
    public static int getAndroidAPI() {
        return Build.VERSION.SDK_INT;
    }

    static {
        System.loadLibrary("onionmasq_mobile");
    }
}

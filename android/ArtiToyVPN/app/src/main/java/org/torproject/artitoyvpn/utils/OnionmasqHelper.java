package org.torproject.artitoyvpn.utils;

import android.annotation.TargetApi;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.net.ConnectivityManager;
import android.os.Build;
import android.os.IBinder;

import org.torproject.artitoyvpn.ui.logging.LogObservable;
import org.torproject.artitoyvpn.vpn.ArtiVpnService;

import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.UnknownHostException;

public class OnionmasqHelper {

    private static OnionmasqHelper instance;
    private final ConnectivityManager connectivityManager;
    private final Context appContext;
    private ArtiVpnService.ArtiVpnServiceBinder serviceBinder;
    private final ServiceConnection connection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName className,
                                       IBinder service) {
            serviceBinder =  (ArtiVpnService.ArtiVpnServiceBinder) service;
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            serviceBinder = null;
        }
    };

    public static OnionmasqHelper getInstance() throws IllegalStateException {
        if (instance == null) {
            throw new IllegalStateException("OnionmasqHelper is not initialized");
        }
        return instance;
    }

    public static void init(Context context) {
        if (instance == null) {
            instance = new OnionmasqHelper(context);
        }
    }

    private OnionmasqHelper(Context context) {
        this.appContext = context.getApplicationContext();
        this.connectivityManager = (ConnectivityManager) appContext.getSystemService(Context.CONNECTIVITY_SERVICE);
    }


    @TargetApi(Build.VERSION_CODES.Q)
    public static int getConnectionOwnerUid(int protocol, byte[] rawSourceAddress, int sourcePort, byte[] rawDestinationAddress, int destinationPort) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            try {
                InetSocketAddress sourceSocketAddress = new InetSocketAddress(InetAddress.getByAddress(rawSourceAddress), sourcePort);
                InetSocketAddress destinationSocketAddress = new InetSocketAddress(InetAddress.getByAddress(rawDestinationAddress), destinationPort);
                return getInstance().connectivityManager.getConnectionOwnerUid(protocol, sourceSocketAddress, destinationSocketAddress);
            } catch (UnknownHostException | IllegalStateException e) {
                e.printStackTrace();
            }
        }

        return -1; // Process.INVALID_UID
    }

    public static void bindVPNService() {
        Intent intent = new Intent(getInstance().appContext, ArtiVpnService.class);
        getInstance().appContext.bindService(intent, getInstance().connection, Context.BIND_AUTO_CREATE);
    }

    public static void unbindVPNService() {
        getInstance().appContext.unbindService(getInstance().connection);
    }

    public static boolean protect(int socket)  {
        if (getInstance().serviceBinder == null) {
            LogObservable.getInstance().addLog("Cannot protect Socket " + socket + ". VpnService is not registered.");
            return false;
        }

        ArtiVpnService service = getInstance().serviceBinder.getService();
        return service.protect(socket);
    }
}

use arti_client::DangerouslyIntoTorAddr;
use arti_client::TorAddr;
use arti_client::TorClient;
use log::info;
use std::net::IpAddr;
use tokio::io::{AsyncReadExt, AsyncWriteExt};

use crate::dns::LockedDnsCookies;
use crate::runtime::OnionTunnelArtiRuntime;
use crate::socket::TcpSocket;

pub struct ArtiProxy {
    socket: TcpSocket,
    arti: TorClient<OnionTunnelArtiRuntime>,
    cookies: LockedDnsCookies,
}

impl ArtiProxy {
    pub fn new(
        socket: TcpSocket,
        arti: TorClient<OnionTunnelArtiRuntime>,
        cookies: LockedDnsCookies,
    ) -> Self {
        Self {
            socket,
            arti,
            cookies,
        }
    }

    pub async fn start(&mut self) {
        info!("Starting Arti Proxy");

        let dest = self.socket.dest();
        let tor_addr;

        if let Some(hostname) = self.cookies.lock().unwrap().get(&dest.0) {
            tor_addr = TorAddr::from((hostname.clone(), dest.1));
        } else {
            let ip_addr: IpAddr = dest.0.into();
            tor_addr = (ip_addr, dest.1).into_tor_addr_dangerously();
        }

        info!("Connecting to: {:?}", tor_addr.as_ref().unwrap());
        let mut arti_stream = self.arti.connect(tor_addr.as_ref().unwrap()).await.unwrap();
        info!("Connected to: {:?}", tor_addr.as_ref().unwrap());

        loop {
            let mut arti_buf = Vec::new();
            let mut tun_buf = Vec::new();

            tokio::select! {
                r = self.socket.read_buf(&mut tun_buf) => match r {
                    Ok(n) => {
                        if n > 0 {
                            let ret = arti_stream.write_all(tun_buf.as_slice()).await;
                            let _ = arti_stream.flush().await;
                            info!("Write to arti: {:?}", ret);
                        }
                    }
                    Err(_) => break,
                },
                r = arti_stream.read_buf(&mut arti_buf) => match r {
                    Ok(n) => {
                        if n > 0 {
                            let ret = self.socket.write(arti_buf.as_slice()).await;
                            info!("Write to onioni0: {:?}", ret);
                        }
                    }
                    Err(_) => break,
                }
            };
        }
    }
}

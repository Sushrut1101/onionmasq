#![allow(non_snake_case)]

#[cfg(target_os = "android")]
use android_logger::Config;
#[cfg(target_os = "android")]
use futures::channel::oneshot;
#[cfg(target_os = "android")]
use jni::objects::{JClass, JObject, JString, JValue};
#[cfg(target_os = "android")]
use jni::{JNIEnv, JavaVM};
use log::Level;
use log::{debug, error};
#[cfg(target_os = "android")]
use once_cell::sync::OnceCell;
use onion_tunnel::OnionTunnel;
use std::fmt::{Debug, Formatter, Result};
use std::net::IpAddr;

static GLOBAL_JVM_PROVIDER: OnceCell<JvmProvider> = OnceCell::new();
const HOLDER_FIELD_NAME: &str = "exitPointer";

pub struct JvmProvider {
    jvm: JavaVM,
}

impl Debug for JvmProvider {
    fn fmt(&self, f: &mut Formatter<'_>) -> Result {
        f.debug_struct("JvmProvider")
            .field("jvm", &"JavaVM")
            .finish()
    }
}

impl JvmProvider {
    pub fn getJavaVM() -> &'static JavaVM {
        let provider = GLOBAL_JVM_PROVIDER
            .get()
            .expect("JvmProvider is not initialized");
        &provider.jvm
    }

    pub fn new(jvm: JavaVM) -> Self {
        let jvm = jvm;
        Self { jvm }
    }
}

#[cfg(target_os = "android")]
#[no_mangle]
pub extern "C" fn Java_org_torproject_OnionMasq_runProxy(
    env: JNIEnv,
    _: JClass,
    fd: i32,
    interface_name: JString,
    cache_dir: JString,
    data_dir: JString,
    exit_pointer: JObject,
) {
    let apiVersion = getAndroidAPI();
    debug!("Onionmasq_runProxy on Android API {}", apiVersion);
    let rt = tokio::runtime::Runtime::new().expect("failed to create tokio runtime");
    let cache_dir = env.get_string(cache_dir).expect("cache_dir is invalid");
    let cache_dir = cache_dir.to_string_lossy();
    let data_dir = env.get_string(data_dir).expect("data_dir is invalid");
    let data_dir = data_dir.to_string_lossy();

    // the onshot channel created here is stored on java and is accessible in different methods without storing it globally.
    let (sender, receiver) = oneshot::channel::<i32>();

    let _ = match env.set_rust_field(exit_pointer, HOLDER_FIELD_NAME, sender) {
        Ok(()) => {
            //debug log
            println!("exit pointer stored in java");
        }
        Err(error) => {
            println!("Error in storing exit pointer in java: {:?}", error);
        }
    };

    let config = onion_tunnel::TorClientConfigBuilder::from_directories(
        format!("{}/arti-data", data_dir),
        format!("{}/arti-cache", cache_dir),
    )
    .build()
    .unwrap();
    rt.block_on(async move {
        debug!("creating onion_tunnel...");

        let mut onion_tunnel: OnionTunnel;
        match OnionTunnel::create_with_fd(
            &env.get_string(interface_name)
                .expect("interface_name is invalid")
                .to_string_lossy(),
            fd,
            config,
        )
        .await
        {
            Ok(v) => {
                debug!("successfully created tun interface");
                onion_tunnel = v;
            }
            Err(e) => {
                debug!("couldn't start onionmasq proxy ({:?})", e);
                return;
            }
        }
        debug!("starting onionmasq...");
        _ = onion_tunnel.start().await;
        _ = receiver.await;
        debug!("stopped onionmasq...");
    });
}

#[no_mangle]
pub extern "C" fn Java_org_torproject_OnionMasq_closeProxy(
    env: JNIEnv,
    _: JClass,
    exit_pointer: JObject,
) {
    debug!("closing proxy...");

    // Retriving sender to trigger close
    match env.take_rust_field::<JObject, &str, futures::channel::oneshot::Sender<i32>>(
        exit_pointer,
        HOLDER_FIELD_NAME,
    ) {
        Ok(sender) => {
            // debug!("is cancelled {}", sender.is_canceled());
            match sender.send(1) {
                Ok(()) => {
                    debug!("send OK");
                }
                Err(error) => {
                    debug!("send FAILED");
                    debug!("{:?}", error);
                }
            }
        }
        Err(error) => debug!("take_rust_field error {:?}", error),
    }
}

#[cfg(target_os = "android")]
#[no_mangle]
pub extern "C" fn Java_org_torproject_OnionMasq_init(env: JNIEnv, _: JClass) {
    if GLOBAL_JVM_PROVIDER.get().is_none() {
        android_logger::init_once(
            Config::default()
                .with_min_level(Level::Trace)
                .with_tag("onionmasq"),
        );
        debug!("logger initialized");
        // `JNIEnv` cannot be sent across thread boundaries. To be able to use JNI
        // functions in other threads, we must first obtain the `JavaVM` interface
        // which, unlike `JNIEnv` is `Send`.
        let jvm = env.get_java_vm().unwrap();
        GLOBAL_JVM_PROVIDER.set(JvmProvider::new(jvm)).unwrap();
    } else {
        error!("init is only allowed to be called once")
    }
}

pub fn getAndroidAPI() -> i32 {
    let local_jvm = JvmProvider::getJavaVM();
    let guard = local_jvm.attach_current_thread().unwrap();
    let class = guard
        .find_class("org/torproject/OnionMasq")
        .expect("Failed to load the target class");
    match guard.call_static_method(class, "getAndroidAPI", "()I", &[]) {
        Ok(result) => result.i().unwrap() as i32,
        _Error => -1,
    }
}

pub fn protect(socket: i32) -> bool {
    let local_jvm = JvmProvider::getJavaVM();
    let guard = local_jvm.attach_current_thread().unwrap();
    let class = guard
        .find_class("org/torproject/OnionMasq")
        .expect("Failed to load the target class");

    match guard.call_static_method(class, "protect", "(I)Z", &[JValue::from(socket)]) {
        Ok(result) => result.z().unwrap() as bool,
        _Error => false,
    }
}

pub fn getConnectionOwnerUid(
    protocol: i32,
    source_address: IpAddr,
    source_port: i32,
    destination_address: IpAddr,
    destination_port: i32,
) -> i32 {
    let local_jvm = JvmProvider::getJavaVM();

    let guard = local_jvm.attach_current_thread().unwrap();
    let class = guard
        .find_class("org/torproject/OnionMasq")
        .expect("Failed to load the target class");

    let source_ip_bytes = match source_address {
        IpAddr::V4(source_address) => source_address.octets().to_vec(),
        IpAddr::V6(source_address) => source_address.octets().to_vec(),
    };
    let destination_ip_bytes = match destination_address {
        IpAddr::V4(destination_address) => destination_address.octets().to_vec(),
        IpAddr::V6(destination_address) => destination_address.octets().to_vec(),
    };

    let source_address_jbyte_array = guard
        .byte_array_from_slice(source_ip_bytes.as_slice())
        .unwrap();
    let destination_address_jbyte_array = guard
        .byte_array_from_slice(destination_ip_bytes.as_slice())
        .unwrap();

    match guard.call_static_method(
        class,
        "getConnectionOwnerUid",
        "(I[BI[BI)I",
        &[
            JValue::from(protocol),
            JValue::from(source_address_jbyte_array),
            JValue::from(source_port),
            JValue::from(destination_address_jbyte_array),
            JValue::from(destination_port),
        ],
    ) {
        Ok(result) => result.i().unwrap() as i32,
        _Error => -1,
    }
}

fn main() {
    if std::env::var("CARGO_CFG_TARGET_OS").unwrap() != "android" {
        return;
    }

    // for some reason if we don't explicitly add this argument, the library get linked in a way
    // its symbols are not exported, so they are still considered missing when loading
    // libonionmasq-mobile.so
    println!("cargo:rustc-link-arg=-lbionic-missing");

    cc::Build::new()
        .file("bionic-missing/all-missing.c")
        .include("bionic-missing/include/")
        .include("bionic-missing/src/internal")
        .include(include_arch())
        .flag("-Wno-parentheses")
        .compile("libbionic-missing.a");
}

fn include_arch() -> &'static str {
    match std::env::var("CARGO_CFG_TARGET_ARCH").unwrap().as_str() {
        "arm" => "bionic-missing/arch/arm",
        "aarch64" => "bionic-missing/arch/aarch64",
        "x86" => "bionic-missing/arch/i386",
        "x86_64" => "bionic-missing/arch/x86_64",
        _ => unimplemented!(),
    }
}
